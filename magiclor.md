# docker 的注意事项

- alpine
- latest

> `alpine` 最简的 linux 系统
> `latest` 最新版本（默认）

# docker 镜像源

```json
{
  "registry-mirrors": [
    "https://registry.docker-cn.com",
    "http://hub-mirror.c.163.com",
    "https://docker.mirrors.ustc.edu.cn",
    "https://cr.console.aliyun.com"
  ],
  "insecure-registries": [],
  "debug": true,
  "experimental": false
}
```

# docker-compose 构建应用

`docker-compose up -d`

# 查看镜像

`docker images`

# 容器
## 启动容器

`docker run -it --name ceshi_php php`

## 查看容器

`docker ps -a`

## 运行容器

`docker exec -it ubuntu /bin/bash`

## 容器相关

```mermaid
granph TD

```
